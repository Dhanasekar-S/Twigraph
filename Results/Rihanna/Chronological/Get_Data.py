from os import listdir
from os.path import isfile, join
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
import re
import nltk

numbers=['0','1','2','3','4','5','6','7','8','9']
files_in_dir = [f for f in listdir('/home/warft/Desktop/politics') if isfile(join('/home/warft/Desktop/politics', f))]
path = "/home/warft/Desktop/politics/"
each_profile_data = []
data = ""
stop = set(stopwords.words('english'))
eset = {'you', 'we', 'it', 'get'}

#files_in_dir=list(set(files_in_dir))

for file in files_in_dir:
    data = ""
    with open(path + file, 'r', encoding="utf8") as f:
        for line in f:
            line = [i for i in str(line).lower().split() if i not in stop]
            line = [i for i in str(line).lower().split() if not str(i).__contains__("you")]
            line = [i for i in str(line).lower().split() if
                    not str(i).__contains__("http") and not str(i).__contains__("rt") and not str(i).__contains__("#") and not str(i).__contains__("thanks") and not str(i).__contains__("tonight")]
            line = [i for i in str(line).lower().split()  if not any(substring in i for substring in numbers)]
            line = [re.sub(r'\b\w{1,5}\b', '', i) for i in str(line).lower().split()]
            data += str(line)
    each_profile_data.append(data)

for i in files_in_dir:
    print(files_in_dir.index(i),i)
# stemmer = SnowballStemmer("english")
# eachprofiledata1=[]
# for i in each_profile_data:
#     data1=""
#     data1+=str(i)
#     singles = [stemmer.stem(plural.strip()) for plural in data1.split(" ")]
#     data1+=str(singles)
#     eachprofiledata1.append(data1)
#
# print(stemmer.stem("apologizing"))
def get_data():
    return each_profile_data
    # count=0
    # for i in each_profile_data:
    #     print(i)
    #     print("over \n")
    #     print(count)
    #     count+=1
